package com.techuniversity.hackathon;

import com.kastkode.springsandwich.filter.annotation.Before;
import com.kastkode.springsandwich.filter.annotation.BeforeElement;
import com.techuniversity.hackathon.Repositorio.RetiroDao;
import com.techuniversity.hackathon.Retiro.Retiro;
import com.techuniversity.hackathon.security.AuthHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/retiros")
public class RetirosController {

    @Autowired
    RetiroDao retiroDao;

    @GetMapping (path = "/{funBecario}", headers = {"Authorization"})
    @Before(@BeforeElement(AuthHandler.class))
    @CrossOrigin
    public ResponseEntity<Retiro> getRetiro (@PathVariable String funBecario){
        Retiro retiro = retiroDao.generateRetiro(funBecario);
        if( retiro == null){
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok().body(retiro);
        }
    }

}
