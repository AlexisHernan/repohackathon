package com.techuniversity.hackathon;

import com.kastkode.springsandwich.filter.annotation.Before;
import com.kastkode.springsandwich.filter.annotation.BeforeElement;
import com.techuniversity.hackathon.Becario.Becario;
import com.techuniversity.hackathon.Becario.Becarios;
import com.techuniversity.hackathon.Repositorio.BecarioDao;
import com.techuniversity.hackathon.Retiro.Retiro;
import com.techuniversity.hackathon.security.AuthHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping(path = "/becarios")
public class BecariosController {

    @Autowired
    BecarioDao becarioDao;

    // get becarios
    @CrossOrigin
    @GetMapping(path = "/", headers = {"Authorization"})
    @Before(@BeforeElement(AuthHandler.class))
    public Becarios getBecarios(){
        return becarioDao.getAllBecarios();
    }

    //get becario
    @CrossOrigin
    @GetMapping (path = "/{id}", headers = {"Authorization"})
    @Before(@BeforeElement(AuthHandler.class))
    public ResponseEntity<Becario> getBecario (@PathVariable String id){
        Becario becario = becarioDao.getBecario(id);
        if( becario == null){
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok().body(becario);
        }
    }

    //historial
    @CrossOrigin
    @GetMapping(path = "/{id}/retiro", headers = {"Authorization"})
    @Before(@BeforeElement(AuthHandler.class))
    public ResponseEntity<List<Retiro>> getRetiroBecario (@PathVariable String id){
        return ResponseEntity.ok().body(becarioDao.getRetiroBecario(id));
    }


}
