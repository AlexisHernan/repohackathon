package com.techuniversity.hackathon.Repositorio;

import com.techuniversity.hackathon.Becario.*;
import com.techuniversity.hackathon.Retiro.Retiro;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class BecarioDao {
    @Autowired
    RetiroDao retiroDao;

    private static Becarios list = new Becarios();
    static {

        ResumenPago sumary1 = new ResumenPago( 12, 3, 4, 1600.00);
        ResumenPago sumary2 = new ResumenPago( 12, 2, 4, 1300.00);
        ResumenPago sumary3 = new ResumenPago( 12, 2, 4, 1300.00);

        list.getListaBecarios().add(new Becario("1",
                "axel lopez rodriguez",
                14,"alexis@bbva.com","secundaria",
                "hugo lopez jimenez",
                "telesecundaria","activo", sumary1));

        list.getListaBecarios().add(new Becario("2",
                "norberto lopez rodriguez",
                15,"lorena@bbva.com",
                "secundaria",
                "pepe lopez jimenez",
                "telesecundaria",
                "activo", sumary2));

        list.getListaBecarios().add(new Becario("3",
                "Alexis Hernandez",
                16,"",
                "secundaria",
                "Martin Hernandez",
                "EST 113",
                "activo", sumary3));

    }

    public Becarios getAllBecarios(){
        return list;
    }


    public Becario getBecario (String id){
        for (Becario becario : list.getListaBecarios()){
            if(becario.getFunBecario().equals(id) ){
                return becario;
            }
        }

        return null;
    }

    public List<Retiro> getRetiroBecario (String id){
        Becario current = getBecario(id);
        List<Retiro> retirosList = new ArrayList<>();

        if (current !=null){
            for (Retiro retiro : retiroDao.getAllRetiros().getListaRetiros()){
                if(retiro.getFunBecario().equals(id)){
                    retirosList.add(retiro);
                }
            }
        }
        return retirosList;
    }

    public Becario getBecarioByCorreo (String correo){
        for (Becario becario : list.getListaBecarios()){
            if(becario.getCorreo().equals(correo) ){
                return becario;
            }
        }

        return null;
    }
}
