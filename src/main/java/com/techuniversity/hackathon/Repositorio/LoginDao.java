package com.techuniversity.hackathon.Repositorio;

import com.techuniversity.hackathon.Login.Login;
import com.techuniversity.hackathon.Login.Logins;
import org.springframework.stereotype.Repository;

@Repository
public class LoginDao {

    private static Logins list = new Logins();
    static {

        list.getListaLogin().add(new Login("alexis@bbva.com",
                "12345"));

        list.getListaLogin().add(new Login("lorena@bbva.com",
                "12345"));



    }


    public Login getLogin (Login objetoLogin){

        for (Login log : list.getListaLogin()){
            if(log.getUsuario().equals(objetoLogin.getUsuario()) && log.getContraseña().equals(objetoLogin.getContraseña()) ){
                return log;
            }
        }

        return null;
    }
}
