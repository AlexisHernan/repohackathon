package com.techuniversity.hackathon.Repositorio;

import com.techuniversity.hackathon.Becario.Becario;
import com.techuniversity.hackathon.Becario.Becarios;
import com.techuniversity.hackathon.Becario.ResumenPago;
import com.techuniversity.hackathon.Retiro.Retiro;
import com.techuniversity.hackathon.Retiro.Retiros;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Repository
public class RetiroDao {
    @Autowired
    BecarioDao becarioDao;

    private static Retiros listaRetiro = new Retiros();
    static {
        listaRetiro.getListaRetiros().add(new Retiro("1", "1", "1459-8576-9823",
                "3919",
                3200.00,
                "05-02-2021",
                "12-02-2021",
                "COBRADO"));

        listaRetiro.getListaRetiros().add(new Retiro(
                "2",
                "1",
                "5987-2369-8514",
                "4081",
                1600.00,
                "06-03-2020",
                "13-03-2021",
                "COBRADO"));

        listaRetiro.getListaRetiros().add(new Retiro(
                "3",
                "2",
                "8496-5763-2546",
                "3321",
                1300.00,
                "06-01-2020",
                "13-01-2021",
                "COBRADO"));

        listaRetiro.getListaRetiros().add(new Retiro(
                "4",
                "2",
                "8576-3298-4561",
                "2740",
                1300.00,
                "08-02-2020",
                "15-02-2021",
                "COBRADO"));

        listaRetiro.getListaRetiros().add(new Retiro(
                "5",
                "2",
                "7659-3298-0061",
                "0040",
                1300.00,
                "05-03-2020",
                "12-03-2021",
                "CADUCADO"));


    }

    public Retiros getAllRetiros(){
        return listaRetiro;
    }

    public void addRetiro(Retiro retiro) {
        listaRetiro.getListaRetiros().add(retiro);
    }

    public Retiro generateRetiro (String funBecario){
        int i = 0;
        Retiro result = new Retiro();
        Becario bec = becarioDao.getBecario(funBecario);
        StringBuilder codigo = new StringBuilder();
        LocalDateTime myDateObj = LocalDateTime.now();
        DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd-MM-yyyy");

        for (i = 0; i < 3; i++) {
            int rand = (int)(Math.random() * 9999) + 1000;
            if(String.valueOf(rand).length() != 4){
                i--;
            }else{
                codigo.append(rand).append("-");
            }

        }
        codigo.toString();

        int clave = 0;
        while( clave == 0 ){
            clave = (int)(Math.random() * 9999) + 1000;
            if ( String.valueOf( clave ).length() != 4){
                clave = 0;
            }
        }

        result.setCodigo(codigo.substring( 0, codigo.length() - 2));
        result.setClaveSeguridad(String.valueOf( clave ));
        result.setFechaExpedicion(myDateObj.format(myFormatObj));
        result.setFecha_vencimiento( (myDateObj.plusDays(7).format(myFormatObj)));
        result.setId(String.valueOf(getAllRetiros().getListaRetiros().size()+1));
        result.setStatus("ACTIVO");
        result.setMonto(bec.getResumenPago().getMonto() *
                (bec.getResumenPago().getMesActual() - bec.getResumenPago().getMesesPagados()));
        result.setFunBecario(funBecario);

        addRetiro(result);

        return result;
    }
}
