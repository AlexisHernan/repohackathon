package com.techuniversity.hackathon.Becario;

public class Becario {
    private String funBecario;
    private String nombre;
    private int edad;
    private String correo;
    private String tipo_de_beca;
    private String tutor;
    private String escuela;
    private String Status;
    private ResumenPago resumenPago;

    public Becario() {
    }

    public Becario(String funBecario, String nombre, int edad, String correo, String tipo_de_beca, String tutor, String escuela, String status, ResumenPago resumenPago) {
        this.funBecario = funBecario;
        this.nombre = nombre;
        this.edad = edad;
        this.correo = correo;
        this.tipo_de_beca = tipo_de_beca;
        this.tutor = tutor;
        this.escuela = escuela;
        Status = status;
        this.resumenPago = resumenPago;
    }

    public String getFunBecario() {
        return funBecario;
    }

    public void setFunBecario(String funBecario) {
        this.funBecario = funBecario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTipo_de_beca() {
        return tipo_de_beca;
    }

    public void setTipo_de_beca(String tipo_de_beca) {
        this.tipo_de_beca = tipo_de_beca;
    }

    public String getTutor() {
        return tutor;
    }

    public void setTutor(String tutor) {
        this.tutor = tutor;
    }

    public String getEscuela() {
        return escuela;
    }

    public void setEscuela(String escuela) {
        this.escuela = escuela;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public ResumenPago getResumenPago() {
        return resumenPago;
    }

    public void setResumenPago(ResumenPago resumenPago) {
        this.resumenPago = resumenPago;
    }

    @Override
    public String toString() {
        return "Becario{" +
                "funBecario='" + funBecario + '\'' +
                ", nombre='" + nombre + '\'' +
                ", edad=" + edad +
                ", correo='" + correo + '\'' +
                ", tipo_de_beca='" + tipo_de_beca + '\'' +
                ", tutor='" + tutor + '\'' +
                ", escuela='" + escuela + '\'' +
                ", Status='" + Status + '\'' +
                ", resumenPago=" + resumenPago +
                '}';
    }
}