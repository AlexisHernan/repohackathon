package com.techuniversity.hackathon.Becario;

import java.util.ArrayList;
import java.util.List;

public class Becarios {

    private List<Becario> listaBecarios;

    public List<Becario> getListaBecarios(){
        if(listaBecarios == null){
            listaBecarios = new ArrayList<>();
        }

        return listaBecarios;
    }
}
