package com.techuniversity.hackathon.Becario;

public class ResumenPago {
    private int totMeses;
    private int mesesPagados;
    private int mesActual;
    private Double monto;

    public ResumenPago(){

    }

    public ResumenPago(int totMeses, int mesesPagados, int mesActual, Double monto) {
        this.totMeses = totMeses;
        this.mesesPagados = mesesPagados;
        this.mesActual = mesActual;
        this.monto = monto;
    }

    public int getTotMeses() {
        return totMeses;
    }

    public void setTotMeses(int totMeses) {
        this.totMeses = totMeses;
    }

    public int getMesesPagados() {
        return mesesPagados;
    }

    public void setMesesPagados(int mesesPagados) {
        this.mesesPagados = mesesPagados;
    }

    public int getMesActual() {
        return mesActual;
    }

    public void setMesActual(int mesActual) {
        this.mesActual = mesActual;
    }

    public Double getMonto() {
        return monto;
    }

    public void setMonto(Double monto) {
        this.monto = monto;
    }

    @Override
    public String toString() {
        return "ResumenPago{" +
                "totMeses=" + totMeses +
                ", mesesPagados=" + mesesPagados +
                ", mesActual='" + mesActual + '\'' +
                ", monto=" + monto +
                '}';
    }
}
