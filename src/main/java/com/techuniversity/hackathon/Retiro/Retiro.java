package com.techuniversity.hackathon.Retiro;

public class Retiro {
    private String id;
    private String funBecario;
    private String codigo;
    private String claveSeguridad;
    private Double monto;
    private String fechaExpedicion;
    private String fecha_vencimiento;
    private String status;

    public Retiro(){

    }

    public Retiro(String id, String funBecario, String codigo, String claveSeguridad, Double monto, String fechaExpedicion, String fecha_vencimiento, String status) {
        this.id = id;
        this.funBecario = funBecario;
        this.codigo = codigo;
        this.claveSeguridad = claveSeguridad;
        this.monto = monto;
        this.fechaExpedicion = fechaExpedicion;
        this.fecha_vencimiento = fecha_vencimiento;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFunBecario() {
        return funBecario;
    }

    public void setFunBecario(String funBecario) {
        this.funBecario = funBecario;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getClaveSeguridad() {
        return claveSeguridad;
    }

    public void setClaveSeguridad(String claveSeguridad) {
        this.claveSeguridad = claveSeguridad;
    }

    public Double getMonto() {
        return monto;
    }

    public void setMonto(Double monto) {
        this.monto = monto;
    }

    public String getFechaExpedicion() {
        return fechaExpedicion;
    }

    public void setFechaExpedicion(String fechaExpedicion) {
        this.fechaExpedicion = fechaExpedicion;
    }

    public String getFecha_vencimiento() {
        return fecha_vencimiento;
    }

    public void setFecha_vencimiento(String fecha_vencimiento) {
        this.fecha_vencimiento = fecha_vencimiento;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "RetiroDao{" +
                "id='" + id + '\'' +
                ", funBecario='" + funBecario + '\'' +
                ", codigo='" + codigo + '\'' +
                ", claveSeguridad='" + claveSeguridad + '\'' +
                ", monto=" + monto +
                ", fechaExpedicion='" + fechaExpedicion + '\'' +
                ", fecha_vencimiento='" + fecha_vencimiento + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
