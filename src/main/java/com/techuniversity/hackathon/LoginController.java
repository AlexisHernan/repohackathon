package com.techuniversity.hackathon;


import com.kastkode.springsandwich.filter.annotation.Before;
import com.kastkode.springsandwich.filter.annotation.BeforeElement;
import com.techuniversity.hackathon.Becario.Becario;
import com.techuniversity.hackathon.Login.Login;
import com.techuniversity.hackathon.Repositorio.BecarioDao;
import com.techuniversity.hackathon.Repositorio.LoginDao;
import com.techuniversity.hackathon.security.AuthHandler;
import com.techuniversity.hackathon.security.JWTBuilder;
import org.apache.coyote.Response;
import org.jose4j.json.internal.json_simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@RestController
@RequestMapping(path = "/login")
public class LoginController {

    @Autowired
    LoginDao loginDao;

    @Autowired
    BecarioDao becarioDao;

    @Autowired
    JWTBuilder jwtBuilder;

    @CrossOrigin
    @PostMapping(path = "/")
    public ResponseEntity<Object> login (@RequestBody Login log) {
        Login logs = loginDao.getLogin(log);
        if(logs != null) {

            Becario becario = becarioDao.getBecarioByCorreo(log.getUsuario());

            if( becario != null){
                URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                        .path("/{id}")
                        .buildAndExpand(becario.getFunBecario())
                        .toUri()
                        ;
                String cadena = jwtBuilder.generateToken(log.getUsuario(),"admin");
                JSONObject obj = new JSONObject();
                obj.put("location",location);
                obj.put("jwt",cadena);

//                HttpHeaders responseHeaders = new HttpHeaders();
//                responseHeaders.set("Location",
//                        obj.get("location").toString());
//                responseHeaders.set("jwt",
//                        obj.get("jwt").toString());

              return ResponseEntity.ok(obj.toJSONString());

                //return  ResponseEntity.ok().headers(responseHeaders).body("");


            } else {
                return ResponseEntity.ok().body(null);
            }




        } else{
            return ResponseEntity.badRequest().build();
        }

    }





}
